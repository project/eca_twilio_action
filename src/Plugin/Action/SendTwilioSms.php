<?php

namespace Drupal\eca_twilio_action\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\twilio\Services\Sms;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a custom action to send SMS via Twilio.
 *
 * @Action(
 *   id = "send_twilio_sms",
 *   label = @Translation("Send Twilio SMS")
 * )
 */
class SendTwilioSms extends ConfigurableActionBase {

  /**
   * The Twilio SMS service.
   *
   * @var \Drupal\twilio\Services\Sms
   */
  protected Sms $twilioSms;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    /** @var \Drupal\eca_twilio_action\Plugin\Action\SendTwilioSms $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->twilioSms = $container->get('twilio.sms');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Replace tokens in the configuration values
    $phone_number = $this->tokenServices->replaceClear($this->configuration['phone_number']);
    $message = $this->tokenServices->replaceClear($this->configuration['message']);
    // Decode HTML entities in the message
    $decoded_message = html_entity_decode($message);

    try {
      $this->twilioSms->messageSend($phone_number, $decoded_message);
      \Drupal::logger('eca_twilio_action')->info('Message sent successfully to @phone_number', ['@phone_number' => $phone_number]);
    } catch (Exception $e) {
      \Drupal::logger('eca_twilio_action')->error('Error sending message: @message', ['@message' => $e->getMessage()]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'phone_number' => '',
      'message' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Number'),
      '#default_value' => $this->configuration['phone_number'],
      '#description' => $this->t('The phone number to send the SMS to.'),
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value' => $this->configuration['message'],
      '#description' => $this->t('The message to send.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['phone_number'] = $form_state->getValue('phone_number');
    $this->configuration['message'] = $form_state->getValue('message');
    parent::submitConfigurationForm($form, $form_state);
  }
}